#!/bin/bash
set -ehv
if [ -d dist ]; then
	mkdir .tmp
	mv -v dist/favicon.{ico,png}.bin .tmp;
	rm -r dist;
fi
cp -r src dist
mv dist/favicon.svg{,.bin}
if [ -d .tmp ]; then
	mv -v .tmp/* dist;
	rmdir -v .tmp
else
	convert -resize 128x src/favicon.svg dist/favicon.ico;
	convert -resize 512x src/favicon.svg dist/favicon.png;
	oxipng -v dist/favicon.png
	mv -v dist/favicon.ico{,.bin}
	mv -v dist/favicon.png{,.bin}
fi
mv -v dist/{cnt.html,index.html.bin}
for file in user.js style.css; do
	mv -v dist/$file dist/$file.txt
done
