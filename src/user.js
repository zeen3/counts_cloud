document.body.classList.remove('nojs');
const $ = q => document.querySelector(q);
const _cnt = $('#cnt');
const delta0 = $('#dlt').value;
const itime = d => new Date(d || Date.now()).toISOString().slice(0, 19);
const on_message = m => {
	let s = '';
	switch (m._t) {
		case 1:
			// initial session population
			s = `now online: ${m.online} (connect${m.old ? 'ed' : 'ing'})`;
			break;
		case 2:
			// renamed user
			s = `renamed: ${m.prev} renamed to ${m.online}`;
			break;
		case 3:
			// update val
			sval(m.value);
			switch (Math.sign(m.delta)) {
				case 0: return;
				case +1: s='+';
			}
			s = `${m.name} \u0394${s}${m.delta} (value now ${m.value})`;
			break;
		case 4:
			// reserved
			break;
		case 5:
			// offline user
			s = `${m.offline} disconnected at ${itime(m.at)}`;
			break
		case 420:
			// calm down please
			s = `rate limit hit, please wait ${Math.ceil(m.time / 1000)} seconds and retry.`;
			console.warn(s);
			alert(s);
			break;
		case 127:
			// excessive message length. Only happens if people b stupid
			s = `got error 413: excessive message length`
			break;

		// acknowledgements
		case -1:
			// acc connect
			s = `connected as ${m.named}`;
			name = m.named;
			sval(m.value);
			break;
		case -2:
			// ack rename
			s = `renamed self: ${m.named}`;
			name = m.named;
			console.log(s);
			break;
		case -3:
			// ack delta
			s = `acknowledge delta: ${m.delta}`;
			sdel(-m.delta, null, m.time);
			break;
		case -4:
			// nack delta: time too high
			s = `negative ack from ${itime(m.time)} at ${itime(m.recv_at)} (time higher than session max by ${m.time - m.mtime})`;
			sdel(-m.delta, null, m.time);
			console.warn(s);
			break;

		// internal
		case -128:
			const {delta, time} = m;
			if (delta === 0) return;
			s = `sent ${delta > 0 ? "+" : ""}${delta} @ ${itime(time)}`
			if (ready) _send({ time, delta });
			let _v = setTimeout(() => {
				if (waiting.has(time)) mk_new_sock();
			}, 10000);
			break;
		case -127:
			s = `connection was opened at ${itime(0)}`;
			fails = 0;
			break;
		case -126:
			s = `connection was closed, attempting reconnection...`;
			mk_new_sock();
			break;
		case -125:
			s = `connection had error (@${Math.round(m.e.timeStamp)}, ${itime(0)})`;
			break;
		case -124:
			s = `went offline, connection may close`;
			break;
		case -123:
			s = `went online, reconnecting`;
			break;
		case -122:
			s = `connection was aborted due to too many failures`;
			break;
	}

	if (s) {
		++hst.start;
		// history entry
		let e = hst.children.length > 63 ? hst.children[63] : document.createElement("li");
		e.innerText = s;
		hst.prepend(e);
	}
}

// set up the delta
let delta = 0, name = 'anon';
console.log({
	get cnt() { return BigInt(_cnt.value); },
	get delta() { return delta },
	set delta(v) { sdel(v, 1) },
	get name() { return name },
	set name(v) { _send({ name: String(v) }); },
	info: 'use delta = n to add n to delta',
});
let sp = false;
if ('registerProperty' in CSS) {
	sp = true;
	CSS.registerProperty({
		name: "--scale",
		syntax: "<number>",
		inherits: false,
		initialValue: 1,
	});
}
const set_scaling = (e, l) => {
	let scaling = 1;
	if (l >= 4) {
		scaling = 2 / (l + 1);
	} else if (l > 1) {
		scaling = (l - 1) / l;
	}
	sp ? e.style.setProperty('--scale', scaling) : e.style.setProperty('font-size', scaling * 7 + 'ch');
}
// delta value
let sdel = (v, send, time = Date.now()) => {
	delta += v;
	switch (Math.sign(delta)) {
		case 0:  dlt.value = delta0; break;
		case -1: dlt.value = delta; break;
		case +1: dlt.value = '+' + delta; break;
	};
	set_scaling(dlt, dlt.value.length);
	send && on_message({_t: -128, delta: v, send, time});
	if (!waiting.delete(time)) waiting.set(time, v);
};
let sval = v => {
	_cnt.value = String(v);
	cnt = BigInt(v);
	set_scaling(_cnt, _cnt.value.length);
}
// add1/sub1/optionsmenu/qrcode
document.body.addEventListener("click", e => {
	switch (e.target.id) {
		case "add": sdel(+1, 1); break;
		case "sub": sdel(-1, 1); break;
		case "opt": dlg.show(); break;
		case "qr": {
			if (document.fullscreenElement) {
				document.exitFullscreen();
			} else {
				qrcode.requestFullscreen();
			}
			return;
		}
	}
}, {
	capture: true,
	passive: true,
})
let _id = $("#id");
_id = _id.value;
console.log(_id);
if (/^[0-9a-f]{64}$/.test(_id)) history.replaceState(
	{setup: true},
	document.title,
	'/' + _id,
);

const mk_sock = () => Object.assign(
	new WebSocket(`wss://${location.hostname}/api/cnt/${_id}/sock`),
	{
		binaryType: "arraybuffer",
		onmessage(m) {
			on_message(JSON.parse(m.data));
		},
		onopen(e) {
			on_message({_t: -127})
			this.send(`{"ready":true}`);
		},
		onclose(e) {
			on_message({_t: -126});
			ready = false;
		},
		onerror: e => on_message({_t: -125, e}),
	},
)
const _ready = () => {
	const sorted = Array.from(waiting).sort((a, b) => a[0] - b[0]);
	for (const [time, delta] of sorted) _send({time, delta});
	ready = true;
	sock.removeEventListener('open', _ready);
}
let ready = false;
const _send = data => sock.send(JSON.stringify(data));
const mk_new_sock = async () => {
	if (fails++ < 3) {
		try { sock.close(); } catch (e) {console.log(e);}
		Object.assign(sock, {
			onmessage: null,
			onopen: null,
			onclose: null,
			onerror: null,
		});
		await new Promise(r => setTimeout(r, 1000));
		sock = mk_sock();
		sock.addEventListener('open', _ready, {once: true});
	} else {
		alert("Reached max fails, aborted.");
		on_message({_t: -122});
	}
}
let fails = 0;
let waiting = new Map;
let sock = mk_sock();
sock.addEventListener('open', _ready, {once: true});
ononline = () => {
	on_message({_t: -123});
};
onoffline = () => on_message({_t: -124});
