import HTML from "./index.html.bin";
import ICON_ICO from "./favicon.ico.bin";
import ICON_SVG from "./favicon.svg.bin";
import ICON_PNG from "./favicon.png.bin";
import STYLE_CSS from "./style.css.txt";
import USER_JS from "./user.js.txt";
const stat = async n => new Response(await fetch(`https://http.cat/${n}.jpg`), {status: n});

export default {
	async fetch(request, env) {
		try {
			return await handleRequest(request, env)
		} catch (e) {
			let v = {ts: e.toString()};
			for (const k of ['stack','message','lineNumber','columnNumber']) v[k] = e[k] ?? null;
			e = JSON.stringify(v);
			if (request.headers.get('upgrade') == "websocket") {
				let pair = new WebSocketPair();
				pair[1].accept();
				pair[1].send(e);
				pair[1].close(1011, "Uncaught exception during setup");
				return new Response(null, {status: 101, webSocket: pair[0]});
			} else {
				return new Response(e, {status: 500});
			}
		}
	},
}
class Inliner {
	element(e) {
		const opts = {html: true};
		switch (e.tagName) {
			case 'link':
				e.replace(`<style>${STYLE_CSS}</style>`,opts);
				return;
			case 'script':
				e.removeAttribute('src');
				e.setInnerContent(USER_JS, opts);
				return;
		}
	}
}
class TextRep {
	constructor(val) {this.val = val;}
	async element(e) {
		let val = await this.val;
		switch (e.tagName) {
			case 'img': return e.setAttribute('src', '/qr_raw/qz=1/ec=l?HTTPS://IT.COUNTS.CLOUD/' + val.toUpperCase());
			case 'input': return e.setAttribute('value', val);
			case 'meta': return e.setAttrubute('content', val);
			case 'a': return e.setAttribute('href', `/api/cnt/${val}/get`)
			case 'time':
				val = val.toISOString()
				e.setAttribute('datetime', val);
			default: return e.setInnerContent(val);
		}
	}
}
const re_id = /^[0-9a-f]{64}$/;
const resp = (tx, type) => new Response(tx, {headers: {"content-type": type}});
async function d_html(id, cnt) {
	const html = new Response(HTML, {
		"headers": {
			"content-type": "text/html;charset=UTF-8",
		}
	});
	let inliner = new Inliner;
	return new HTMLRewriter()
		.on('link[href="style.css"]', inliner)
		.on('script[src="user.js"]', inliner)
		.on(".cnt", new TextRep(cnt))
		.on(".id", new TextRep(id))
		.on("time", new TextRep(new Date))
		.transform(html);
}
async function handleRequest(req, env) {
	let url = new URL(req.url);
	let [first, ...rest] = url.pathname.toLowerCase().slice(1).split('/');
	switch (first) {
		// static

		case "style.css": return resp(STYLE_CSS, "text/css");
		case "user.js": return resp(USER_JS, "application/javascript");

		// icons
		case "favicon.ico": return resp(ICON_ICO, "image/vnd.microsoft.icon");
		case "favicon.svg": return resp(ICON_SVG, "image/svg+xml");
		case "favicon.png": return resp(ICON_PNG, "image/png");

		// dynamic
		case "api": return api(rest, req, env, url.queryParams);
		// should never happen
		case "qr": {
			const [_id] = rest;
			if (!re_id.test(_id)) return stat(404);
			const id = env.cnts.idFromString(_id);
			return qr(url, id.toString());
		}
		// index
		case "": {
			const id = env.cnts.newUniqueId();
			return d_html(id.toString(), '0');
		}
	}
	if (re_id.test(first)) {
		const id = env.cnts.idFromString(first);
		let room = env.cnts.get(id);
		let cnt = room.fetch(`/get`)
			.then(cnt => cnt.status === 200 ? cnt.text() : "0")
			.catch(_ => '0');
		return d_html(id.toString(), cnt);
	}
	return stat(404);
}
async function qr({origin}, id) {
	return await fetch(`/qr_raw/?https://it.counts.cloud/${id}`)
}
async function api([route, id, q], req, env, qs) {
	switch (route) {
		default: return stat(404);
		case "cnt": {
			if (id == null) {
				return stat(405); // method not allowed
			} else {
				if (re_id.test(id)) {
					id = env.cnts.idFromString(id);
				} else {
					return stat(406);
				}
				let room = env.cnts.get(id);
				let url = new URL(req.url);
				url.pathname = "/" + q;
				return await room.fetch(url, req);
			}
		}
	}
}
const send = (data, ws) => ws.send(JSON.stringify(data));
export class Room {
	constructor(state, env) {
		this.store = state.storage;
		this.env = env;
		this.sess = [];
		this.last_ts = 0;
		this.init_p = null;
	}
	async init() {
		let stored = await this.store.get(["value", "users"]);
		this.value = BigInt.asIntN(256, stored.get("value") || 0n) || 0n;
		this.users = BigInt.asUintN(256, stored.get("users") || 0n) || 0n;
	}
	async save() {
		await this.store.put({
			value: BigInt.asIntN(256, this.value),
			users: BigInt.asUintN(256, this.users),
		});
	}

	async fetch(req) {
		let url = new URL(req.url);
		switch (url.pathname) {
			case "/sock": {
				this.init_p ??= this.init().catch(e => {
					this.init_p = null;
					throw e;
				});
				await this.init_p;
				if (req.headers.get("upgrade") !== "websocket") {
					return stat(426);
				}
				let ip = req.headers.get("CF-Connecting-IP");
				let ws = new WebSocketPair();
				await this.session_setup(ws[1], ip);
				return new Response(null, {status: 101, webSocket: ws[0]});
			};
			case "/get": if ('value' in this) {
				return new Response(this.value.toString());
			} else {
				return new Response('DISCONNECTED', {status: 300});
			};
			case "/reset":
				this.init_p ??= this.init().catch(e => {
					this.init_p = null;
					throw e;
				});
				await this.init_p;
				Object.assign(this, {value: 0n});
				await this.save();
				return stat(204);
			default: return stat(404);
		}
	}
	async session_setup(ws, ip) {
		ws.accept();
		let ip_id = this.env.limits.idFromName(ip);
		let limiter = new RateLimiterClient(
			() => this.env.limits.get(ip_id),
			e => ws.close(1011, e.stack)
		);
		send({ready: true}, ws);
		let uidx = ++this.users;
		let store_idx = this.store.put("users", uidx);

		let sess = {
			ws,
			quit: false,
			name: `anon-${uidx}`,
			ip,
			msgs: [],
			time: 0,
		};
		for (const online of this.sess) {
			sess.msgs.push({
				_t: 1,
				online: online.name || 'anon',
				old: 1,
			});
		}
		this.sess.push(sess);
		this.broadcast({_t: 1, online: sess.name, old: 0});

		sess.msgs.push({
			_t: -1,
			named: sess.name,
			value: this.value.toString(),
		});

		let named = false;
		ws.addEventListener("message", async msg => {
			try {
				if (sess.quit) {
					ws.close(1011, "WebSocket broken.");
					return;
				}
				if (!limiter.checkLimit()) return send({_t: 420});
				// we want short messages please
				if (msg.data.length > 127) {
					ws.send(`{"_t":127,"err":413}`);
					return;
				}
				// meh json is fine
				let data = JSON.parse(msg.data);
				if (data.ready) {
					sess.msgs.forEach(d => send(d, ws));
					delete sess.msgs;
				}
				if (data.name) {
					let prev = sess.name;
					sess.name = String(data.name);
					sess.time = +data.time || this.now();
					this.broadcast({
						_t: 2,
						online: sess.name,
						prev,
					});
					named = true;
					send(ws, {
						_t: -2,
						named: sess.name,
						value: this.value.toString(),
					});
					return;
				}
				if (sess.time >= data.time) {
					data = {
						_t: -4,
						delta: Number(delta),
						value: this.value.toString(),
						time: Number(data.time),
						recv_at: this.now(),
						mtime: Number(sess.time),
					}
					send({_t: -4, ...data}, ws);
					return;
				}
				let delta = Math.max(-65536, Math.min(data.delta || 0, 2 ** 16 - 1));
				delta = BigInt.asIntN(16, BigInt(delta)) || 0n;
				let {value} = this;
				value += delta;
				this.value = BigInt.asIntN(256, value);
				data = {
					_t: -3,
					delta: Number(delta),
					value: this.value.toString(),
					time: Number(data.time || this.now()),
					recv_at: this.now(),
				}
				sess.time = data.time;
				send(data, ws);
				let d = delta && this.store.put("value", this.value);
				this.broadcast(Object.assign(
					data,
					{ _t: 3, name: sess.name },
				));
				await d;
			} catch(e) {
				send({err: 500, msg: e.message}, ws);
			}
		});
		ws.addEventListener("close", async () => {
			this.sess = this.sess.filter(s => s !== sess);
		});
		await store_idx;
	}
	now() {
		return (this.last_ts = Math.max(Date.now(), this.last_ts + 1));
	}
	broadcast(e) {
		let offline = [];
		this.sess = this.sess.filter(s => {
			if (s.name) {
				try {
					send(e, s.ws);
					return true;
				} catch(e) {
					s.quit = true;
					offline.push(s);
				}
			} else {
				// no lurk
				s.msgs.push(e);
			}
		});
		for (const dis of offline) if (name) {
			let msg = {_t: 5, offline: dis.name, at: this.now()};
			for (const sess of this.sess) try {
				send(msg, sess.ws)
			} catch (e) {
				sess.quit = true;
			};
		}
	}
}



// =======================================================================================
// The RateLimiter Durable Object class.

// RateLimiter implements a Durable Object that tracks the frequency of messages from a particular
// source and decides when messages should be dropped because the source is sending too many
// messages.
//
// We utilize this in Room, above, to apply a per-IP-address rate limit. These limits are
// global, i.e. they apply across all rooms, so if a user spams one room, they will find
// themselves rate limited in all other rooms simultaneously.
export class RateLimiter {
  constructor(controller, env) {
    // Timestamp at which this IP will next be allowed to send a message. Start in the distant
    // past, i.e. the IP can send a message now.
    this.nat = 0;
  }

  // Our protocol is: POST when the IP performs an action, or GET to simply read the current limit.
  // Either way, the result is the number of seconds to wait before allowing the IP to perform its
  // next action.
  async fetch(request) {
		let now = Date.now();

		this.nat = Math.max(now, this.nat);

		if (request.method == "POST") {
			// POST request means the user performed an action.
			// We allow one action per 100ms.
			this.nat += 100;
		}

		// Return the number of seconds that the client needs to wait.
		//
		// We provide a "grace" period of a few seconds, meaning that the client can make a handful of requests
		// in a quick burst before they start being limited.
		let cooldown = Math.max(0, this.nat - now - 3500);
		return new Response(Uint32Array.of(cooldown));
  }
}

// RateLimiterClient implements rate limiting logic on the caller's side.
class RateLimiterClient {
  // The constructor takes two functions:
  // * getLimiterStub() returns a new Durable Object stub for the RateLimiter object that manages
  //   the limit. This may be called multiple times as needed to reconnect, if the connection is
  //   lost.
  // * reportError(err) is called when something goes wrong and the rate limiter is broken. It
  //   should probably disconnect the client, so that they can reconnect and start over.
  constructor(getLimiterStub, reportError) {
    this.getLimiterStub = getLimiterStub;
    this.reportError = reportError;

    // Call the callback to get the initial stub.
    this.limiter = getLimiterStub();

    // When `inCooldown` is true, the rate limit is currently applied and checkLimit() will return
    // false.
    this.inCooldown = false;
  }

  // Call checkLimit() when a message is received to decide if it should be blocked due to the
  // rate limit. Returns `true` if the message should be accepted, `false` to reject.
  checkLimit() {
    if (this.inCooldown) {
      return false;
    }
    this.inCooldown = true;
    this.callLimiter();
    return true;
  }

  // callLimiter() is an internal method which talks to the rate limiter.
  async callLimiter() {
    try {
      let response;
      try {
        // Currently, fetch() needs a valid URL even though it's not actually going to the
        // internet. We may loosen this in the future to accept an arbitrary string. But for now,
        // we have to provide a dummy URL that will be ignored at the other end anyway.
        response = await this.limiter.fetch("https://dummy-url", {method: "POST"});
      } catch (err) {
        // `fetch()` threw an exception. This is probably because the limiter has been
        // disconnected. Stubs implement E-order semantics, meaning that calls to the same stub
        // are delivered to the remote object in order, until the stub becomes disconnected, after
        // which point all further calls fail. This guarantee makes a lot of complex interaction
        // patterns easier, but it means we must be prepared for the occasional disconnect, as
        // networks are inherently unreliable.
        //
        // Anyway, get a new limiter and try again. If it fails again, something else is probably
        // wrong.
        this.limiter = this.getLimiterStub();
        response = await this.limiter.fetch("https://dummy-url", {method: "POST"});
      }

      // The response indicates how long we want to pause before accepting more requests.
      let [cooldown] = new Uint32Array(await response.arrayBuffer());
      await new Promise(resolve => setTimeout(resolve, cooldown));

      // Done waiting.
      this.inCooldown = false;
    } catch (err) {
      this.reportError(err);
    }
  }
}
